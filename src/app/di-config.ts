import { Container } from "inversify";
import { AppConfig } from "./services/config";
import { AuthService } from "./services/auth";
import { NewsService } from "./services/news";

export const rootContainer = new Container();

// service bindings to IoC container
rootContainer
  .bind<AppConfig>(AppConfig)
  .toSelf()
  .inSingletonScope();
rootContainer
  .bind<AuthService>(AuthService)
  .toSelf()
  .inSingletonScope();
rootContainer
  .bind<NewsService>(NewsService)
  .toSelf()
  .inSingletonScope();
