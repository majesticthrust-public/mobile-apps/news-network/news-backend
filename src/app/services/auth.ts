import { injectable } from "inversify";
import { AppConfig } from "./config";
import sqlite3 from "better-sqlite3";

// NOTE keep api async in case of database engine change

/**
 * Provides an access to the auth database.
 */
@injectable()
export class AuthService {
  private database: sqlite3.Database;

  constructor(private config: AppConfig) {
    // config.authDB
    this.database = sqlite3(config.authDB, {
      // TODO logging?
      // verbose: logging_function
    });

    this.init();
  }

  /**
   * Check if the given API key is valid.
   * @param apiKey
   */
  public async isAuthorized(apiKey: string): Promise<boolean> {
    const stmt = this.database.prepare(`select (api_key) from Users`);
    const row = stmt.get();

    if (row != null) {
      return row.api_key == apiKey;
    } else {
      return false;
    }
  }

  /**
   * Initialize database, creating tables if necessary.
   */
  private init() {
    this.database.pragma("journal_mode = WAL");

    const stmt = this.database.prepare(`
      create table if not exists Users (
        id integer primary key autoincrement,
        name text not NULL,
        api_key text unique not NULL
      )
    `);
    stmt.run();
  }
}
