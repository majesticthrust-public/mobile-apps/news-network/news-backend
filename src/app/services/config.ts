import { injectable } from "inversify";
import YAML from "yaml";
import fs from "fs-extra";
import path from "path";

interface Config {
  auth_db: string;
  news_db_folder: string;
  locales: string[];
  port: number;
}

@injectable()
export class AppConfig {
  /**
   * Path to the auth database
   */
  public authDB: string = "";

  /**
   * Path to the dir that contains news databases
   */
  public newsDBDir: string = "";

  /**
   * Enabled locales
   */
  public locales: string[] = [];

  /**
   * Server port
   */
  public port: number = -1;

  public load_cascade(paths: string[]) {
    // filter out invalid paths
    paths = paths.filter(p => p != null);

    for (const configPath of paths) {
      if (!fs.existsSync(configPath)) {
        console.log(
          `Config at path "${configPath}" does not exist! Trying next...`
        );
        continue;
      }

      const stat = fs.statSync(configPath);
      if (!stat.isFile()) {
        console.log(`Path "${configPath}" is not a file! Trying next...`);
        continue;
      }

      try {
        this.load(configPath);
        // loaded config successfully, can return
        return;
      } catch (e) {
        console.log(`Unable to load config at path ${configPath}:`, e);
        continue;
      }
    }

    throw new Error(
      "Unable to load config from any of the following paths:\n" +
        paths.join("\n")
    );
  }

  public load(configPath: string) {
    const str = fs.readFileSync(configPath).toString();
    const config: Config = YAML.parse(str);

    // process entries before assignment so that any errors would happen before assignment
    const configDir = path.dirname(configPath);
    config.auth_db = path.resolve(configDir, config.auth_db);
    config.news_db_folder = path.resolve(configDir, config.news_db_folder);

    // assign entries
    this.authDB = config.auth_db;
    this.newsDBDir = config.news_db_folder;
    this.locales = config.locales;
    this.port = config.port;
  }

  /**
   * Check if the specified locale is enabled
   * @param locale 
   */
  public validLocale(locale: string) {
    return this.locales.includes(locale);
  }
}
