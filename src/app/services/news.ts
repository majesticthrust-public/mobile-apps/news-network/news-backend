import { injectable } from "inversify";
import sqlite3 from "better-sqlite3";
import { AppConfig } from "./config";
import path from "path";

@injectable()
export class NewsService {
  private databases: { [locale: string]: sqlite3.Database } = {};

  constructor(private config: AppConfig) {
    for (const locale of config.locales) {
      const dbPath = path.join(config.newsDBDir, `${locale}.sqlite3`);
      this.databases[locale] = sqlite3(dbPath, {
        // TODO logging?
        // verbose: ...
      });
    }
  }

  /**
   * Check if the specified string is a topic in the database
   * @param topic
   */
  public isTopic(locale: string, topic: string) {
    const db = this.getDB(locale);
    const stmt = db.prepare("select (topic) from Topics where topic = ?");
    const result = stmt.get(topic);

    return result != null;
  }

  /**
   * Get fresh news for specified topic, i.e. sorted by datetime in descending order.
   * @param locale
   * @param topic
   * @param offset
   * @param limit
   */
  public getNewsByTopic(locale: string, topic: string, offset: number, limit: number) {
    const db = this.getDB(locale);
    const stmt = db.prepare(`
      select News.title, source_url, publish_date
      from News
      inner join Events on News.event_id = Events.id
      inner join EventsTopicsJunction on Events.id = EventsTopicsJunction.event_id
      inner join Topics on EventsTopicsJunction.topic_id = Topics.id
      where Topics.topic = @topic
      order by datetime(publish_date) DESC
      limit @limit offset @offset
    `);
    const results: {
      title: string;
      source_url: string;
      publish_date: string;
    }[] = stmt.all({ topic, limit, offset });

    return results;
  }

  /**
   * Get all news, regardless of topic.
   * @param locale 
   * @param offset 
   * @param limit 
   */
  public getAllNews(locale: string, offset: number, limit: number) {
    const db = this.getDB(locale);
    const stmt = db.prepare(`
      select News.title, source_url, publish_date
      from News
      order by datetime(publish_date) DESC
      limit @limit offset @offset
    `);
    const results: {
      title: string;
      source_url: string;
      publish_date: string;
    }[] = stmt.all({ limit, offset });

    return results;
  }

  /**
   * Get database connection for specified locale.
   * If locale is incorrect, throw error.
   * @param locale
   */
  private getDB(locale: string) {
    if (!this.config.validLocale(locale)) {
      throw new Error(`Invalid locale ${locale}`);
    }

    return this.databases[locale];
  }
}
