import express from "express";
import httpError from "http-errors";
import { rootContainer } from "../di-config";
import { AppConfig } from "../services/config";
import { NewsService } from "../services/news";

const router = express.Router();

// TODO change route handlers to classes (to leverage DI properly)
// https://nehalist.io/routing-with-typescript-decorators/

// TODO cache requests in memory for faster responses

/**
 * Get latest news for a given locale and topic
 * Params: locale (like en-US), topic
 */
router.get("/latest", (req, res, next) => {
  const config = rootContainer.get<AppConfig>(AppConfig);
  const news = rootContainer.get<NewsService>(NewsService);

  const locale: string = req.query.locale;
  const topic: string = req.query.topic;

  // TODO move default values to config
  // limit from 1 to 100, default = 10
  const limit: number = Math.min(Math.max(0, req.query.limit || 10), 100);
  const offset: number = req.query.offset || 0;

  // check if locale is enabled
  if (!config.validLocale(locale)) {
    const err = new httpError.BadRequest(`Invalid locale '${locale}'`);
    next(err);
  }

  if (topic != null) {
    // return news for topic
    // check if topic is in the database
    if (!news.isTopic(locale, topic)) {
      const err = new httpError.BadRequest(`Invalid topic '${topic}'`);
      next(err);
    }

    const articles = news.getNewsByTopic(locale, topic, offset, limit);
    res.status(200).json(articles);
  } else {
    // return all news
    const articles = news.getAllNews(locale, offset, limit);
    res.status(200).json(articles);
  }
});

export default router;
