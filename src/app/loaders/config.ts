import { rootContainer } from "../di-config";
import { AppConfig } from "../services/config";

export default async (configPaths: string[]) => {
  const config = rootContainer.get<AppConfig>(AppConfig);
  config.load_cascade(configPaths);
};
