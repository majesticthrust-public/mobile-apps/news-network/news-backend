import express, { Express, Request, Response, NextFunction } from "express";
import passport from "passport";
import { HeaderAPIKeyStrategy } from "passport-headerapikey";
import httpError from "http-errors";
import indexRouter from "../routes";
import { rootContainer } from "../di-config";
import { AuthService } from "../services/auth";

// TODO document with swagger

export default async (expressApp: Express) => {
  expressApp.use(passport.initialize());

  const auth = rootContainer.get<AuthService>(AuthService);

  passport.use(
    new HeaderAPIKeyStrategy(
      { header: "Authorization", prefix: "Api-Key " },
      false,
      async (apiKey, done) => {
        try {
          const authorized = await auth.isAuthorized(apiKey);
          // TODO get user and pass to `done` callback
          done(null, authorized);
        } catch (e) {
          done(e);
        }
      }
    )
  );

  // authenticate all requests
  expressApp.use(
    passport.authenticate("headerapikey", {
      session: false
    })
  );

  // TODO do we really need that?
  expressApp.use(express.json());

  // set up routes
  expressApp.use("/", indexRouter);

  // catch 404 and forward to error handler
  expressApp.use(function(req, res, next) {
    next(new httpError.NotFound());
  });

  // default error handler
  expressApp.use(
    (
      err: httpError.HttpError,
      req: Request,
      res: Response,
      next: NextFunction
    ) => {
      res.status(err.status).json({
        message: err.message,
        route: req.originalUrl
      });
    }
  );

  // TODO logging?
};
