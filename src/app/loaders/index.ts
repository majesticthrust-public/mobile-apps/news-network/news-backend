import { Express } from "express";

import initAppConfig from "./config";
import initExpress from "./express";

export interface InitArgs {
  configPaths: string[];
  expressApp: Express;
}

/**
 * Initializes the app using specified params.
 * @param args initialization params
 */
export async function initializeApp(args: InitArgs) {
  await initAppConfig(args.configPaths);
  await initExpress(args.expressApp);
}
