import commander from "commander";
import express from "express";
import { initializeApp } from "./loaders";
import { rootContainer } from "./di-config";
import { AppConfig } from "./services/config";

/**
 * Main function. Initializes and runs the app.
 * @param args Commander object for CLI args
 */
export async function main(args: commander.Command) {
  const app = express();

  // init everything
  await initializeApp({
    expressApp: app,
    configPaths: [
      args.config as string,
      "/usr/local/news-network-system/config.yaml"
    ]
  });

  const config = rootContainer.get<AppConfig>(AppConfig);

  // start listening
  app.listen(config.port, () => {
    console.log(`App is listening on port ${config.port}`)
  });
}
