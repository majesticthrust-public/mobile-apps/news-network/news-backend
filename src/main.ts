/**
 * Bootstrapping code
 */

// polyfill for inversify
import "reflect-metadata";

import commander from "commander";
import { main } from "./app/app";

const program = new commander.Command();

program.option("-c, --config <path>", "Config file path");
program.parse(process.argv);

main(program);
